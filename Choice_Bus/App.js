import React from 'react';
import {View, Text, StyleSheet,TouchableOpacity,ScrollView} from 'react-native';
import Modal from 'react-native-modal';
import {bus} from './data.js';
import {route} from './data_route.js'
import haversine from "haversine";
import {createSwitchNavigator, createAppContainer} from 'react-navigation';
import * as TaskManager from 'expo-task-manager';
import * as Location from 'expo-location';
import * as Permissions from 'expo-permissions';
let ligne_bus;
let ensemble_bus = new Array();
let latitude0=14.74532952863207,longitude0= -17.401078906745852;
// let latitude0,longitude0
let list_destination = new Array();
let lat,lng;


const LOCATION_TASK_NAME = 'background-location-task';



 class Ligne extends React.Component{

    constructor(props){
        super(props);

        this.state={
            hasLocationPermissions: false,
            visibleModal: null,
            list: [],
            ligne: null
        }
    }

  async componentDidMount(){
    const { status } = await Permissions.askAsync(Permissions.LOCATION);
    this.setState({ hasLocationPermissions: status === 'granted' });
    
    await Location.startLocationUpdatesAsync(LOCATION_TASK_NAME, {
      accuracy: Location.Accuracy.Balanced,
      timeInterval: 1000,
    });

    // await  navigator.geolocation.getCurrentPosition(
    //        (position) => {
    //            console.log("position", position)
    //        },
    //        (error) => alert(error.message),
    //        { enableHighAccuracy: true, timeout: 20000, maximumAge: 1000 }
    //     );
  }

    onPress = () =>{
      console.log(latitude0)
      console.log(longitude0)
            this.setState({ visibleModal: 1 })
            bus.map((data)=>{
                result = parseFloat(haversine(
                    p1 = { latitude:  latitude0,longitude:longitude0},
                    p2 = { latitude: data.Stop.lat, longitude: data.Stop.lng}
                    )).toFixed(3) * 1000

                if(result<500){
                         if(data.Stop.lat==p2.latitude && data.Stop.lng==p2.longitude){
                           ensemble_bus.push(...new Set(data.buses));  
                     }
                }
               
            })

            ensemble_bus.sort();
            console.log([...new Set(ensemble_bus)])
            this.setState({
                list: [...new Set(ensemble_bus)]
            })
    }

     onPress1(){
        console.log(this.state.ligne)
        ligne_bus=this.state.ligne;
        this.props.navigation.navigate("Destination")

    }

    render(){
      const { hasLocationPermissions } = this.state;
      if (hasLocationPermissions === null) {
        return <Text>Requesting for location permission</Text>;
      }
      if (hasLocationPermissions === false) {
        return <Text>No access to location</Text>;
      }
        return(
            <View style={styles.container}>
                   <TouchableOpacity
                        style={styles.button}
                        onPress={this.onPress}
                        disabled={false}
                    >
                        <Text>Choisir Une Ligne</Text>
                    </TouchableOpacity>

                <Modal isVisible={this.state.visibleModal === 1}>
                  <View style={styles.modalContent}>
                            <ScrollView>

                            {this.state.list.map(item => (
              
                                <TouchableOpacity 
                                style={styles.button}
                                onPress={async ()=>{await this.setState({ligne: item}),await this.onPress1()}}
                                key={item}
                                >
                                 <Text>{item}</Text> 
                                </TouchableOpacity>
                           ))
                           }
                           
                        </ScrollView>
                    </View>
                 </Modal> 
               
            </View>
            
        )
       
    }
}

class Destination extends React.Component{

  state={
    visibleModal: null,
}

  onPress = () =>{
    this.setState({ visibleModal: 1 })
    route.map(data0=>{
      if(data0.bus== ligne_bus){
          console.log(data0.route[0].lat)
          result1 = parseFloat(haversine(
              p1 = { latitude: latitude0,longitude:longitude0},
              p2 = { latitude: data0.route[0].lat, longitude: data0.route[0].lng}
              )).toFixed(3) * 1000

          result2 = parseFloat(haversine(
                  p1 = { latitude: latitude0,longitude:longitude0},
                  p2 = { latitude: data0.route[data0.route.length-1].lat, longitude: data0.route[data0.route.length-1].lng}
                  )).toFixed(3) * 1000

                  console.log({"result1":result1,"result2":result2})
                  if(result1<500){
                    list_destination.push('aller')
                  }else if (result2<500){
                    list_destination.push('retour')
                  }else{
                    list_destination.push('aller','retour')
                  }
      
      }
  })
}
  render(){
    return(
          <View style={styles.container}>
          <TouchableOpacity
              style={styles.button}
              onPress={this.onPress}
          >
              <Text>Choisir Destination</Text>
          </TouchableOpacity>

      <Modal isVisible={this.state.visibleModal === 1}>
        <View style={styles.modalContent}>
        <ScrollView>

            {list_destination.map(item => (

                <TouchableOpacity 
                style={styles.button}
                onPress={()=>{}}
                key={item}
                >
                <Text>{item}</Text> 
                </TouchableOpacity>
            ))
            }
        </ScrollView>
          </View>
        </Modal> 
      
    </View>
    )
  }
}
const MainNavigator = createSwitchNavigator({
  Ligne: {screen: Ligne},
  Destination: {screen: Destination},
},
{
  initialRouteName: 'Ligne'
}
);
const App = createAppContainer(MainNavigator);

export default App;

const styles = StyleSheet.create({
    container:{
        flex: 1,
        justifyContent: "center",
        alignItems: "center",
        backgroundColor: "#34495e"
    },
    button:{
        backgroundColor: 'white',
        padding: 12,
        margin: 16,
        justifyContent: 'flex-start',
        alignItems: 'center',
        borderRadius: 4,
        borderColor: 'rgba(0, 0, 0, 0.1)',
    },
    modalContent:{
         flexDirection: "row",
         backgroundColor: '#34495e',
         padding: 5,
         justifyContent: 'space-around',
         borderRadius: 5,
         borderColor: 'rgba(0, 0, 0, 0.1)',
    }
})

TaskManager.defineTask(LOCATION_TASK_NAME, ({ data, error }) => {
  if (error) {
    return;
  }
  if (data) {
    const { locations } = data;
    console.log(locations);
    // latitude0 = locations[0].coords.latitude;
    // longitude0 = locations[0].coords.longitude;
  }
});