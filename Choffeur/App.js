import Home from './Components/Home';
import Login from './Components/Login';
import Register from './Components/Register';
import Acceuil from './Components/Acceuil';
import LoginWithEmailAndPassword from './Components/LoginWithEmailAndPassword';

import {createSwitchNavigator, createAppContainer} from 'react-navigation';

const MainNavigator = createSwitchNavigator({

  Home: {screen: Home},
  Login: {screen: Login},
  Register: {screen: Register},
  Acceuil: {screen: Acceuil},
  LoginWithEmailAndPassword: {screen: LoginWithEmailAndPassword}
},
{
  initialRouteName: 'Home'
}

);

const App = createAppContainer(MainNavigator);

export default App;


