import React from 'react';
import {View, Text, StyleSheet,TouchableOpacity,YellowBox,ScrollView,AsyncStorage} from 'react-native';
import Modal from 'react-native-modal';
import {bus} from './data.js';
import {route} from './data_route.js'
import haversine from "haversine";
import {createSwitchNavigator, createAppContainer} from 'react-navigation';
import * as TaskManager from 'expo-task-manager';
import * as Location from 'expo-location';
import * as Permissions from 'expo-permissions';
import SocketIOClient from 'socket.io-client';
import axios from 'axios';

console.ignoredYellowBox = ['Remote debugger'];
YellowBox.ignoreWarnings([
    'Unrecognized WebSocket connection option(s) `agent`, `perMessageDeflate`, `pfx`, `key`, `passphrase`, `cert`, `ca`, `ciphers`, `rejectUnauthorized`. Did you mean to put these under `headers`?'
]);

let tripid001;
let ligne_bus;
let destination;
let ensemble_bus = new Array();
let latitude0=14.747679866729884,longitude0=-17.3252992772766

const socket = SocketIOClient.connect('http://192.168.0.116:4000');

// const socket = SocketIOClient.connect('http://bus.boki.tech');

const LOCATION_TASK_NAME = 'background-location-task';

class Ligne extends React.Component{

    constructor(props){
        super(props);

        this.state={
            hasLocationPermissions: false,
            visibleModal: null,
            list: [],
            ligne: null,
            destination_bus:null
        }
    }

  async componentDidMount(){

    const { status } = await Permissions.askAsync(Permissions.LOCATION);
    this.setState({ hasLocationPermissions: status === 'granted' });
    
    
  //   await  navigator.geolocation.getCurrentPosition(
  //     (position) => {
  //         console.log("position", position);
  //         latitude0= position.coords.latitude;
  //         longitude0= position.coords.longitude;

  //     },
  //     (error) => alert(error.message),
  //     { enableHighAccuracy: true, timeout: 20000, maximumAge: 1000 }
  //  );
   
  }

    onPress = () =>{

            this.setState({ visibleModal: 1 })

            bus.map((data)=>{
                result = parseFloat(haversine(
                    p1 = { latitude:  latitude0,longitude:longitude0},
                    p2 = { latitude: data.Stop.lat, longitude: data.Stop.lng}
                    )).toFixed(3) * 1000

                if(result<500){
                         if(data.Stop.lat==p2.latitude && data.Stop.lng==p2.longitude){
                           ensemble_bus.push(...new Set(data.buses));  
                     }
                }
               
            })

            ensemble_bus.sort();
            console.log([...new Set(ensemble_bus)])
            this.setState({
                list: [...new Set(ensemble_bus)]
            })
    }

    onPressChoice(){

        ligne_bus=this.state.ligne;
        route.map(data0=>{
            if(data0.bus== ligne_bus){
                // console.log(data0.route[0].lat)
                result1 = parseFloat(haversine(
                    p1 = { latitude: latitude0,longitude:longitude0},
                    p2 = { latitude: data0.route[0].lat, longitude: data0.route[0].lng}
                    )).toFixed(3) * 1000
      
                result2 = parseFloat(haversine(
                        p1 = { latitude: latitude0,longitude:longitude0},
                        p2 = { latitude: data0.route[data0.route.length-1].lat, longitude: data0.route[data0.route.length-1].lng}
                        )).toFixed(3) * 1000
      
                        // console.log({"result1":result1,"result2":result2})
                        if(result1<result2){
                            this.setState({destination_bus: data0.route[data0.route.length-1].name})
                        }else{
                            this.setState({destination_bus : data0.route[0].name})
                        }
  
                       destination = this.state.destination_bus;
            }
        })
        this.props.navigation.navigate("Go")

    }

    render(){
      // console.log(latitude0, longitude0)
      const { hasLocationPermissions } = this.state;
      if (hasLocationPermissions === null) {
        return <Text>Requesting for location permission</Text>;
      }
      if (hasLocationPermissions === false) {
        return <Text>No access to location</Text>;
      }
        return(
            <View style={styles.container}>

            <View style={{
              width: 350,
              height: 400,
              marginVertical: 20,
              backgroundColor: "#16a085",
              borderRadius: 40,
              justifyContent: "center",
              alignItems: 'center'
            }}>
                <Text style={{color:"white", fontSize: 30, fontStyle:"italic",marginVertical: 20, fontWeight: 'bold'}}>Bienvenue!</Text>
               <Text style={{color:"white", fontSize: 20, textAlign: 'center'}}>Veillez choisir une ligne de bus en cliquant sur le boutton ci-dessous.</Text>
       
               <TouchableOpacity
               onPress={this.onPress}
              style={{
               width: 280,
               height: 60,
               marginVertical: 35,
               backgroundColor: "white",
               justifyContent: "center",
               alignItems: 'center'
              }}>
                <Text style={{color:'#16a085', fontWeight: 'bold', fontSize: 20}}>CHOISIR UNE LIGNE</Text>
              </TouchableOpacity>
            </View>
                <Modal isVisible={this.state.visibleModal === 1}>
                  <View style={{
                      backgroundColor: "white",
                       padding: 5,
                       justifyContent: 'space-around',
                       borderRadius: 5,
                       borderColor: 'rgba(0, 0, 0, 0.1)'
                  }}>
                            <ScrollView horizontal>
                            <View style={{
                                width: 350,
                                flexDirection: 'row',
                                flexWrap: 'wrap'
                              }}>
                                  {this.state.list.map(item => (

                                    <TouchableOpacity 
                                    style={{width: 30, height:30, borderRadius: 50, backgroundColor: "#16a085", justifyContent: "center", alignItems: "center",margin:6}}
                                    onPress={async ()=>{await this.setState({ligne: item}),await this.onPressChoice()}}
                                    key={item}
                                    >
                                    <Text style={{color: 'white', fontWeight: 'bold', fontSize: 15}}>{item}</Text> 
                                    </TouchableOpacity>
                                    ))
                                    }
                              </View>
                          
                           
                        </ScrollView>
                    </View>
                 </Modal> 
               
            </View>
            
        )
       
    }
}

class Go extends React.Component{

    
       componentDidMount(){
        
        socket.on('connect', ()=>console.log('Connection'));
      
      }
    
  
      Go =  () => {
          console.log(ligne_bus);
          console.log(destination)
        axios.post('http://192.168.0.116:4000/status0', {
          ligne: ligne_bus,
          destination: destination,
          status: 'en cours',
          date: new Date()
        })
        .then(async function (response) {
         if(response.data.creation){
          tripid001=response.data.tripid_retour
          await Location.startLocationUpdatesAsync(LOCATION_TASK_NAME, {
            accuracy: Location.Accuracy.Highest,
            timeout: 20000, 
            maximumAge: 0
          });
         }
        })
        .catch(function (error) {
          console.log(error);
        });
    
       this.props.navigation.navigate("Stop");
      };
    
      render() {
        return (
        <View style={{
          flex: 1,
          backgroundColor: '#fff',
          alignItems: 'center',
          justifyContent: 'center',
        }}>
         
          <View
          style={{
            width: 350,
            height: 350,
            justifyContent: 'center',
            alignItems: 'center',
            backgroundColor: '#16a085',
            borderRadius: 25,
            marginVertical: 25
          }}>

           
              <Text style={{color: "white", fontSize:15, fontWeight: 'bold', fontStyle: 'italic'}}>LIGNE : {ligne_bus} </Text>
              <Text style={{color: "white", fontSize:15, fontWeight: 'bold', fontStyle: 'italic'}}> DESTINATION : </Text>
              <Text style={{color: "white", fontSize:15, fontWeight: 'bold', fontStyle: 'italic'}}> {destination}</Text>

            <Text style={{color:"white", fontSize: 15,textAlign: 'center', marginVertical: 15}}>Vous pouvez commencez votre voyage </Text>
            <Text style={{color:"white", fontSize: 15,textAlign: 'center'}}>en cliquant sur le boutton ci-dessous.</Text>
            
          <TouchableOpacity
          onPress={this.Go}
            style={{
              justifyContent: 'center',
              alignItems: 'center',
              width: 120,
              height: 120,
              backgroundColor: "white",
              borderRadius: 150,
              marginVertical: 25
            }}
          >
            <Text style={{
              color: '#16a085',
              fontSize: 35,
              fontWeight: 'bold',
            }}>GO</Text>
          </TouchableOpacity>
          </View>
         
        </View>
        );
      }
}


class Stop extends React.Component{

  
    Stop = () => {
      axios.post('http://192.168.0.116:4000/status1', {
        tripid: tripid001
      })
      .then(function (response) {
       if(response.data.status){
        Location.stopLocationUpdatesAsync(LOCATION_TASK_NAME);
       }
      })
      .catch(function (error) {
        console.log(error);
      });
      this.props.navigation.navigate("Out");
    };
  
    render() {
      return (
      <View style={{
        flex: 1,
        backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: 'center',
      }}>
       
        <View
        style={{
          width: 350,
          height: 350,
          justifyContent: 'center',
          alignItems: 'center',
          backgroundColor: '#16a085',
          borderRadius: 25,
          marginVertical: 15
        }}>
              <Text style={{color: "white", fontSize:15, fontWeight: 'bold', fontStyle: 'italic'}}>LIGNE : {ligne_bus} </Text>
              <Text style={{color: "white", fontSize:15, fontWeight: 'bold', fontStyle: 'italic'}}> DESTINATION : </Text>
              <Text style={{color: "white", fontSize:15, fontWeight: 'bold', fontStyle: 'italic'}}>{destination} </Text>

          <Text style={{color:"white", fontSize: 15,marginVertical:15,textAlign: 'center'}}>Vous venez de demmarer votre voyage. </Text>
        <TouchableOpacity
        onPress={this.Stop}
          style={{
            justifyContent: 'center',
            alignItems: 'center',
            width: 120,
            height: 120,
            backgroundColor: "white",
            borderRadius: 150,
            marginVertical: 25
          }}
        >
          <Text style={{
            color: '#16a085',
            fontSize: 35,
            fontWeight: 'bold',
          }}>STOP</Text>
        </TouchableOpacity>
        </View>
       
      </View>
      );
    }
}

class Out extends React.Component{

    Signout= ()=>{
      AsyncStorage.clear(); // to clear the token 
      this.props.navigation.navigate("Home")
   }
  

    render() {
      return (
      <View style={{
        flex: 1,
        backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: 'center',
      }}>
       
        <View
        style={{
          width: 350,
          height: 300,
          justifyContent: 'flex-end',
          alignItems: 'center',
          backgroundColor: '#16a085',
          borderRadius: 25
        }}>

          <Text style={{color:"white", fontSize: 15,textAlign: 'center', marginVertical: 15}}>Vous venez d'arreter votre voyage</Text>
          
              <Text style={{color: "white", fontSize:15, fontWeight: 'bold', fontStyle: 'italic'}}>LIGNE : {ligne_bus} </Text>
              <Text style={{color: "white", fontSize:15, fontWeight: 'bold', fontStyle: 'italic'}}> DESTINATION :</Text>
              <Text style={{color: "white", fontSize:15, fontWeight: 'bold', fontStyle: 'italic'}}>{destination} </Text>

        <TouchableOpacity
        onPress={this.Signout}
          style={{
            justifyContent: 'center',
            alignItems: 'center',
            width: 250,
            height: 40,
            backgroundColor: "#7f8c8d",
            marginVertical: 50
          }}
        >
          <Text style={{
            color: 'white',
            fontSize: 20
          }}>LOG OUT</Text>
        </TouchableOpacity>
        </View>
       
      </View>
      );
    }
}

const MainNavigator = createSwitchNavigator({
  Ligne: {screen: Ligne},
  Go: {screen: Go},
  Stop: {screen: Stop},
  Out: {screen: Out},
},
{
  initialRouteName: 'Ligne'
}
);

const Acceuil = createAppContainer(MainNavigator);

export default Acceuil;

const styles = StyleSheet.create({
    container:{
      flex: 1,
      justifyContent: "center",
      alignItems: "center",
      backgroundColor: "white",
    },
    button:{
        backgroundColor: '#16a085',
        padding: 12,
        margin: 16,
        justifyContent: 'flex-start',
        alignItems: 'center',
        borderRadius: 25
    },
    modalContent:{
         flexDirection: "row",
         backgroundColor: 'white',
         padding: 5,
         justifyContent: 'space-around',
         borderRadius: 25,
         borderColor: 'rgba(0, 0, 0, 0.1)',
    }
})


TaskManager.defineTask(LOCATION_TASK_NAME, ({ data, error }) => {
    if (error) {
      // Error occurred - check `error.message` for more details.
      return;
    }
    if (data) {
      const { locations } = data;
    
      // do something with the locations captured in the background
      console.log("Locations",locations)
      socket.emit(`tripid001`,{data1: locations, tripid: tripid001});
    }
  });