import React, { Component } from 'react';
import {StyleSheet, Text,TouchableOpacity,View,Image} from 'react-native';

export default class Home extends Component {
  _login = () => {
    this.props.navigation.navigate("LoginWithEmailAndPassword")
  }

  _register = () => {
    this.props.navigation.navigate("Register")
  }


  render() {
    return (
      <View style={{flex:1, backgroundColor: "#2c3e50"}}>
        <View style={{justifyContent:"center", alignItems: "center", marginTop: 150}}>
          <Text style={{ fontSize: 40, color: "white"}}>BIENVENUE</Text>
        </View>
        <View style={styles.container}>
          <TouchableOpacity onPress={this._register}>
            <View style={styles.button1}>
              <Text style={styles.buttonText}> + Créer un compte </Text>
            </View>
          </TouchableOpacity>
          <TouchableOpacity onPress={this._login}>
            <View style={styles.button2}>
              <Text style={styles.buttonText}> + S'identifier </Text>
            </View>
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: 60,
    alignItems: "center",
    justifyContent: "flex-end"
  },
  button1: {
    marginBottom: 30,
    width: 350,
    alignItems: 'center',
    backgroundColor: "#25CCF7"
  },
  button2: {
    marginBottom: 30,
    width: 350,
    alignItems: 'center',
    backgroundColor: "#7bed9f"
  },
  buttonText: {
    padding: 30,
    color: 'white',
    fontSize: 25
  }
});




