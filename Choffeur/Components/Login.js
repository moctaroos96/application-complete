import  React from 'react';
import { Text, View ,StyleSheet, Button, Vibration,TouchableOpacity } from 'react-native';
import * as Permissions from 'expo-permissions';
import { BarCodeScanner } from 'expo-barcode-scanner';
import Constants from 'expo-constants';

export default class Login extends React.Component {
 
     // ask for the camara permission
     constructor(props){
       super(props)

       this.state = {
        hasCameraPermission: null,
        scanned: false,
        loggedIn: false
      }
     }

  async componentDidMount() {

    const { status } = await Permissions.askAsync(Permissions.CAMERA);
    this.setState({ hasCameraPermission: status === 'granted' });
  }

  render() {
    // Check for the permission details and render barcode scanner.
     const { hasCameraPermission, scanned } = this.state;
    if (hasCameraPermission === null) {
      return <Text>Requesting for camera permission</Text>;
    }
    if (hasCameraPermission === false) {
      return <Text>No access to camera</Text>;
    }
   
    return (
      <View style={styles.container}>

        <Text style={{fontSize: 15, color:'black', padding: 10, justifyContent: 'flex-start', textAlign: 'center'}}>SCANNEZ VOTRE CODE POUR SE CONNECTER</Text>
        <BarCodeScanner
          onBarCodeScanned={scanned ? undefined : this.handleBarCodeScanned}
          style={{ height: 400, width: 400 }}
          
        />

        {scanned && (
          <Button title={'Appuyez pour scanner à nouveau'} onPress={() => this.setState({ scanned: false })}  style={{marginVertical: 35}}/>
        )}

        <View style={{flexDirection:"row", alignItems:"center", marginVertical: 20}}>
          <Text style={{color: "black", fontSize:17, fontWeight: 'bold'}}>Vous ñ'avez pas un compte ?</Text>
          <TouchableOpacity 
          onPress={()=>{this.props.navigation.navigate("Register")}}
          ><Text style={{color: "#2ed573" ,fontSize:17, fontWeight: 'bold'}}> S'inscrire</Text></TouchableOpacity>
        </View>

      </View>
    );
  }
  // Now need to write readed barcode details.(_handleBarcodeRead function)
  handleBarCodeScanned = ({ type, data }) => {
    this.setState({scanned: true});
    this.setState({username: data.split(" ")[0]});
    this.setState({password: data.split(" ")[1]});
    //  alert(`Bar code with type ${type} and data ${data}  has been scanned!`);
    // If you want vibration after scan
    Vibration.vibrate(100);
    fetch('http://192.168.0.116:4000/users/login', {
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        username: data.split(" ")[0],
        password: data.split(" ")[1]
      })
    })
    .then((response) => response.json())
    .then((res) => {
      console.log(res.token)
      if(res.succes=="1"){
        this.props.navigation.navigate("Acceuil");
      }

      if(res.succes=="2"){
        alert(res.msg);
      }

      if(res.succes=="3"){
        alert(res.msg);
      }
      
    })
    .done();
  };

}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    paddingTop: Constants.statusBarHeight,
    backgroundColor: '#ecf0f1',
  }
});

