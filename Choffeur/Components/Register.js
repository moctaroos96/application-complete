import React, { Component } from 'react';
import {View,TextInput,StyleSheet, TouchableOpacity,Text,KeyboardAvoidingView} from 'react-native';
export default class Register extends Component {
  constructor(props){
    super(props)

    this.state={
      first_name:'',
      last_name:'',
      phone_number:'',
      username:'',
      password:'',
      email:'',
    }
  }
  render() {
      return (
        
        <KeyboardAvoidingView style={styles.container} behavior="padding" enabled>
        
        <Text style={{fontSize: 18, color: 'white', padding: 10}}>CREEZ VOTRE COMPTE ICI !!</Text>
        <TextInput 
        placeholder="Entrez votre prénom"
        style={styles.inputBox}
        onChangeText={(first_name)=>{this.setState({first_name})}}
        value={this.state.first_name}
        keyboardType="default"
        underlineColorAndroid='rgba(0,0,0,0)' 
        placeholderTextColor = "#002f6c"
        selectionColor="#fff"
      />

      <TextInput 
        placeholder="Entrer le nom de famille"
        style={styles.inputBox}
        onChangeText={(last_name)=>{this.setState({last_name})}}
        value={this.state.last_name}
        keyboardType="default"
        underlineColorAndroid='rgba(0,0,0,0)' 
        placeholderTextColor = "#002f6c"
        selectionColor="#fff"
      />

      <TextInput 
        placeholder="Entrez le numéro de téléphone"
        style={styles.inputBox}
        onChangeText={(phone_number)=>{this.setState({phone_number})}}
        value={this.state.phone_number}
        keyboardType="phone-pad"
        underlineColorAndroid='rgba(0,0,0,0)' 
        placeholderTextColor = "#002f6c"
        selectionColor="#fff"
      />

      <TextInput 
        placeholder="Entrer Nom d'utilisateur"
        style={styles.inputBox}
        onChangeText={(username)=>{this.setState({username})}}
        value={this.state.username}
        autoCompleteType="username"
        underlineColorAndroid='rgba(0,0,0,0)' 
        placeholderTextColor = "#002f6c"
        selectionColor="#fff"
      />
      
      <TextInput 
        placeholder="Entrer Mot de passe"
        style={styles.inputBox}
        onChangeText={(password)=>{this.setState({password})}}
        value={this.state.password}
        autoCompleteType="password"
        underlineColorAndroid='rgba(0,0,0,0)' 
        placeholderTextColor = "#002f6c"
        selectionColor="#fff"
        secureTextEntry
      />

      <TextInput 
        placeholder="Entrer Email"
        style={styles.inputBox}
        onChangeText={(email)=>{this.setState({email})}}
        value={this.state.email}
        autoCompleteType="email"
        keyboardType="email-address"
        underlineColorAndroid='rgba(0,0,0,0)' 
        placeholderTextColor = "#002f6c"
        selectionColor="#fff"
      />

  
      <TouchableOpacity 
        style={styles.button}
        onPress={this.onRegister} 
      >
      <Text style={styles.buttonText} >REGISTER</Text>
      </TouchableOpacity>

      <View style={{flexDirection:"row", alignItems:"center", marginVertical: 20}}>
        <Text style={{color: "white", fontSize:17, fontWeight: 'bold'}}>Vous avez un compte ?</Text>
        <TouchableOpacity 
        onPress={()=>{this.props.navigation.navigate("LoginWithEmailAndPassword")}}
        ><Text style={{color: "#25CCF7" ,fontSize:17, fontWeight: 'bold'}}> Se connecter</Text></TouchableOpacity>
      </View>
      
    </KeyboardAvoidingView>
      )
  }


   onRegister =  ()=>{
      fetch('http://192.168.0.116:4000/users/register', {
            method: 'POST',
            headers: {
              'Accept': 'application/json',
              'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                  first_name: this.state.first_name,
                  last_name: this.state.last_name,
                  phone_number: this.state.phone_number,
                  username: this.state.username,
                  password: this.state.password,
                  email: this.state.email
            })
          })
          .then((response) => response.json())
          .then((res) => {
                  if(res.success){
                    alert(res.msg)
                  }

                  if(res.errors){
                    alert(res.msg)
                  }
          })
          .done();
          this.setState({
            first_name:"",
            last_name:"",
            phone_number:"",
            username:"",
            password:"",
            email:""
        })
    }
}


const styles = StyleSheet.create({
  container: {
      flex: 1,
      justifyContent: 'center',
      alignItems: 'center',
      backgroundColor: "#2d3436"
  },
  inputBox: {
      backgroundColor: '#eeeeee', 
      borderRadius: 25,
      paddingHorizontal: 16,
      fontSize: 16,
      color: '#002f6c',
      marginVertical: 10,
      width:300,
      height:45,
  },
  button: {
      height:45,
      flexDirection: 'row',
      justifyContent: 'center',
      alignItems: 'center',
      marginBottom:20,
      width:300,
      borderRadius:30,
      backgroundColor: "#25CCF7"
  },
  buttonText: {
      fontSize: 16,
      fontWeight: '500',
      color: '#ffffff',
      textAlign: 'center'
  }
});
