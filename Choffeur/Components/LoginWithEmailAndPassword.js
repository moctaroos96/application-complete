import  React from 'react';
import { Text, View,TouchableOpacity,TextInput,Image,KeyboardAvoidingView} from 'react-native';

export default class LoginWithEmailAndPassword extends React.Component {
    constructor(props){
        super(props)
    
        this.state={
          username:'',
          password:''
        }
      }

      onLogin = () => {
        fetch('http://192.168.0.116:4000/users/login', {
            method: 'POST',
            headers: {
              'Accept': 'application/json',
              'Content-Type': 'application/json',
            },
            body: JSON.stringify({
              username: this.state.username,
              password: this.state.password
            })
          })
          .then((response) => response.json())
          .then((res) => {
            if(res.succes=="1"){
              this.props.navigation.navigate("Acceuil");
            }

            if(res.succes=="2"){
              alert(res.msg);
            }

            if(res.succes=="3"){
              alert(res.msg);
            }
            
          })
          .done();
      
    this.setState({
      username: '',
      password: ''
    })
  }
  render() {
   
    return (
      <KeyboardAvoidingView style={{
        flex: 1,
        backgroundColor: "#2C3A47",
        alignItems: 'center',
        justifyContent: 'center',
      }}
      behavior="padding" enabled
      >
       
       <Text style={{fontSize: 25, color: 'white', marginVertical: 15}}>S'identifier</Text>
  
       <Text style={{color: 'white', marginVertical: 15}}>--------------</Text>
  
       <TouchableOpacity
          onPress={()=>{this.props.navigation.navigate("Login")}}
            style={{
              justifyContent: 'center',
              alignItems: 'center',
              width: 100,
              height: 100,
              backgroundColor: "white",
              borderRadius: 150,
              marginVertical: 20
            }}
          >
           <Image source={{uri: 'https://ak8.picdn.net/shutterstock/videos/30510538/thumb/6.jpg'}} style={{width: 50, height: 50}} />
          </TouchableOpacity>
  
       <TextInput
          placeholder= 'Entrer votre identifiant'
          onChangeText={(username)=>{this.setState({username})}}
          value={this.state.username}
          style={{
            width: 300,
            height: 55,
            backgroundColor: '#ffffff',
            marginVertical: 25,
            padding: 20
          }}
       />
  
        <TextInput
          placeholder= 'Enter votre Mot de passe'
          autoCompleteType="password"
          onChangeText={(password)=>{this.setState({password})}}
          value={this.state.password}
          style={{
            width: 300,
            height: 55,
            backgroundColor: '#ffffff',
            marginVertical: 5,
            padding: 20
          }}
       />
  
       <TouchableOpacity
       onPress={this.onLogin}
       style={{
          width: 300,
          height: 55,
          backgroundColor: '#7bed9f',
          marginVertical:20,
          justifyContent: "center",
          alignItems: "center"
       }}
       >
         <Text style={{fontSize: 25, color: 'white'}}>Se Connecter</Text>
       </TouchableOpacity>
  
       <View style={{flexDirection:"row", alignItems:"center", marginVertical: 20}}>
        <Text style={{color: "white", fontSize:17, fontWeight: 'bold'}}>Vous ñ'avez pas un compte ?</Text>
        <TouchableOpacity 
        onPress={()=>{this.props.navigation.navigate("Register")}}
        ><Text style={{color: "#7bed9f" ,fontSize:17, fontWeight: 'bold'}}> S'inscrire</Text></TouchableOpacity>
        </View>
       
      </KeyboardAvoidingView>
    );
  }
  
}
