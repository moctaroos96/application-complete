import React from 'react';
import {View, Text, StyleSheet,TouchableOpacity,ScrollView, YellowBox} from 'react-native';
import Modal from 'react-native-modal';
import {bus} from './data.js';
import {route} from './data_route';
import haversine from "haversine";
import {createSwitchNavigator, createAppContainer} from 'react-navigation';
// import * as TaskManager from 'expo-task-manager';
// import * as Location from 'expo-location';
import * as Permissions from 'expo-permissions';
const axios = require('axios');
let ligne_bus;
let ensemble_bus = new Array();
let latitude0=14.747679866729884,longitude0=-17.3252992772766
let latitude1,longitude1
let list_destination = new Array();
// list_destination.push('ALLER','RETOUR');
let tripid002;
let distance_server;
let message_server;
import SocketIOClient from 'socket.io-client';

console.ignoredYellowBox = ['Remote debugger'];
YellowBox.ignoreWarnings([
    'Unrecognized WebSocket connection option(s) `agent`, `perMessageDeflate`, `pfx`, `key`, `passphrase`, `cert`, `ca`, `ciphers`, `rejectUnauthorized`. Did you mean to put these under `headers`?'
]);

class Ligne extends React.Component{

    constructor(props){
        super(props);

        this.state={
            hasLocationPermissions: false,
            visibleModal: null,
            list: [],
            ligne: null,
        }
    }

  async componentDidMount(){
    const { status } = await Permissions.askAsync(Permissions.LOCATION);
    this.setState({ hasLocationPermissions: status === 'granted' });

    // await  navigator.geolocation.getCurrentPosition(
    //        (position) => {
    //            console.log("position", position);
    //            latitude0= position.coords.latitude;
    //            longitude0= position.coords.longitude;
    //        },
    //        (error) => alert(error.message),
    //        { enableHighAccuracy: true, timeout: 20000, maximumAge: 1000 }
    //     );
  }

    onPress = () =>{
            this.setState({ visibleModal: 1 })
            bus.map((data)=>{
                result = parseFloat(haversine(
                    p1 = { latitude:  latitude0,longitude:longitude0},
                    p2 = { latitude: data.Stop.lat, longitude: data.Stop.lng}
                    )).toFixed(3) * 1000

                if(result<500){
                         if(data.Stop.lat==p2.latitude && data.Stop.lng==p2.longitude){
                           ensemble_bus.push(...new Set(data.buses));  
                     }
                }
               
            })

            ensemble_bus.sort();
            console.log([...new Set(ensemble_bus)])
            this.setState({
                list: [...new Set(ensemble_bus)]
            })
    }

     onPressChoice(){
        console.log(this.state.ligne)
        ligne_bus=this.state.ligne;
        route.map(data0=>{
          if(data0.bus== ligne_bus){
            list_destination.push(data0.route[0].name, data0.route[data0.route.length-1].name);
          }
      })
        this.props.navigation.navigate("Destination")
    }

    render(){
      const { hasLocationPermissions } = this.state;
      if (hasLocationPermissions === null) {
        return <Text>Requesting for location permission</Text>;
      }
      if (hasLocationPermissions === false) {
        return <Text>No access to location</Text>;
      }
        return(
            <View style={styles.container}>
              <View style={{
                width:380,
                height: 380,
                borderRadius: 200,
                justifyContent: "center", 
                alignItems: "center",
                backgroundColor: "white"
      }}>
          <Text style={{color:"#1e272e", fontSize: 30, fontStyle:"italic",marginVertical: 25, textAlign: 'center'}}>Bienvenue!</Text>
        <Text style={{color:"#1e272e", fontSize: 20, marginVertical: 15,textAlign: 'center'}}>Veillez choisir une ligne de bus en cliquant sur le boutton ci-dessous.</Text>
                   <TouchableOpacity
                        style={{
                          height:45,
                          flexDirection: 'row',
                          justifyContent: 'center',
                          alignItems: 'center',
                          marginBottom:20,
                          width:250,
                          borderRadius:30,
                          backgroundColor: "#16a085"
                        }}
                        onPress={this.onPress}
                    >
                        <Text  style={{
                                    fontSize: 16,
                                    color: '#ffffff',
                                    textAlign: 'center',
                                    fontWeight: 'bold'
                                }} >CHOISIR UNE LIGNE</Text>
                    </TouchableOpacity>
              </View>
                  

                <Modal isVisible={this.state.visibleModal === 1}>
                  <View style={{
                      backgroundColor: "white",
                       padding: 5,
                       justifyContent: 'space-around',
                       borderRadius: 5,
                       borderColor: 'rgba(0, 0, 0, 0.1)'
                  }}>
                            <ScrollView horizontal  >
                              <View style={{
                                width: 350,
                                flexDirection: 'row',
                                flexWrap: 'wrap'
                              }}>
                               {this.state.list.map(item => (
                  
                                      <TouchableOpacity 
                                      // style={styles.button}
                                      style={{width: 30, height:30, borderRadius: 50, backgroundColor: "#16a085", justifyContent: "center", alignItems: "center",margin:6}}
                                      onPress={async ()=>{await this.setState({ligne: item}),await this.onPressChoice()}}
                                      key={item}
                                      >
                                      <Text style={{color: "white", fontWeight:"bold", fontSize: 15}}>{item}</Text> 
                                      </TouchableOpacity>
                                ))
                                }
                              </View>
                             

                           
                        </ScrollView>
                    </View>
                 </Modal> 
               
            </View>
            
        )
       
    }
}


class Destination extends React.Component{

    constructor(props){
      super(props);

      this.state={
        visibleModal: null,
        sens: null,
        test12345: null,
        distance: null,
        message: null,
      }
        this.demande = this.demande.bind(this)
    }

  demande =async ()=>{
    await axios.post('http://192.168.0.116:4000/demande',{
      ligne: ligne_bus,
      destination: this.state.sens,
    }).then(function(responseJson){
       let res = responseJson.data;
       if(res.success){
        route.map(data0=>{
          data0.route.map(route0=>{
            if(route0.name== this.state.sens){
              // console.log(data0.route[0].lat)
             console.log("hhhhh",route0);
             latitude1= route0.lat;
             longitude1=  route0.lng;
          }
          })
      })
        let dis = haversine(
          p1={latitude: latitude1, longitude: longitude1},
          p2={latitude: res.location.latitude, longitude: res.location.longitude}
          )
          this.setState(
           {
              test12345: res.tripid, 
              distance:dis
           },()=>{
              console.log({
                'tripid': this.state.test12345,
                'distace': this.state.distace
              })
            });
         tripid002=this.state.test12345;
         distance_server= this.state.distance
         
         this.props.navigation.navigate("Bus_Exitse");
      }else{
       console.log(res.msg);
       this.setState({message: res.msg},()=>{
         console.log(this.state.message)
       })
       message_server= this.state.message;
       this.props.navigation.navigate("Pas_De_Bus")
      }
     }.bind(this))
     .catch(function (error) {
       console.log(error);
     }).done();
  }

  onPress = () =>{
    this.setState({ visibleModal: 1 })
}
  render(){
    return(
          <View style={styles.container}>
            <View style={{
                width:380,
                height: 380,
                borderRadius: 200,
                justifyContent: "center", 
                alignItems: "center",
                backgroundColor: "white"
      }}>
         <Text style={{color:"#1e272e", fontSize: 20, marginVertical: 20, marginRight:20, marginLeft:20}}>Veillez choisir une destination de bus en cliquant sur le boutton ci-dessous.</Text>
            <TouchableOpacity
              style={{
                  height:45,
                  flexDirection: 'row',
                  justifyContent: 'center',
                  alignItems: 'center',
                  marginBottom:20,
                  width:250,
                  borderRadius:30,
                  backgroundColor: "#16a085"
              }}
              onPress={this.onPress}
          >
              <Text style={{
                                    fontSize: 16,
                                    fontWeight: '500',
                                    color: '#ffffff',
                                    textAlign: 'center'
                                }} >CHOISIR DESTINATION</Text>
          </TouchableOpacity>
            </View>

      <Modal isVisible={this.state.visibleModal === 1}>
        <View style={styles.modalContent}>
        <ScrollView>

            {list_destination.map(DEST => (

                <TouchableOpacity 
                style={styles.button}
                onPress={async ()=>{await this.setState({sens: DEST}),this.demande()}}
                key={DEST}
                >
                <Text style={{color:"#16a085", fontWeight: "bold"}}>{DEST}</Text> 
                </TouchableOpacity>
            ))
            }
        </ScrollView>
          </View>
        </Modal> 
      
    </View>
    )
  }
}

class Bus_Exitse extends React.Component{

  constructor(props){
    super(props);

    this.state={
      distance: distance_server
    }
  }

  async componentWillMount(){
    const socket = SocketIOClient.connect('http://192.168.0.116:4000');
      console.log(tripid002);
      socket.on(`tripid002`, (data)=>{
        console.log(data);
        dis_restante = haversine(
          p1={latitude:data.data1[0].coords.latitude, longitude: data.data1[0].coords.longitude},
          p2= {latitude: latitude1, longitude: longitude1}
        );
        this.setState({
          distance: dis_restante
        }, ()=>{console.log('distance_restante', this.state.distance)})
    });
  }
  retour = () =>{
    list_destination= []
    this.props.navigation.navigate("Ligne");
  }
  render(){
    return(
      
      <View style={styles.container}>
         <View style={{
                        width:380,
                        height: 380,
                        borderRadius: 200,
                        justifyContent: "center", 
                        alignItems: "center",
                        backgroundColor: "white"
              }}>
              {/* <Text style={{color:"black", fontSize: 30, marginVertical: 20, textAlign: 'center'}}>Bienvenue!!</Text> */}
              <Text style={{color:"black", fontSize: 20, marginVertical: 20,textAlign: 'center'}}>{`le bus est a ${(this.state.distance).toFixed(0)} km de arret.`}</Text>
              <Text style={{color:"black", fontSize: 20,textAlign: 'center'}}>{`le bus arrive  dans ${((this.state.distance/30)*60).toFixed(0)} minutes`}</Text>

              <View style={{flexDirection:"row", alignItems:"center", marginVertical: 15}}>
                  <Text style={{color: "black", fontSize:10, fontWeight: 'bold'}}>Pour retourner vers la page pricipale</Text>
                      <TouchableOpacity 
                      onPress={this.retour}
                  ><Text style={{color: "#16a085" ,fontSize:10, fontWeight: 'bold'}}> Clicke ici</Text></TouchableOpacity>
              </View>
         </View>

      </View>
    )
  }
}

class Pas_De_Bus extends React.Component{
  render(){
    return(
   <View style={styles.container}>
          <View style={{
                      width:380,
                      height: 380,
                      borderRadius: 200,
                      justifyContent: "center", 
                      alignItems: "center",
                      backgroundColor: "white"
            }}>
            <Text style={{color:"black", fontSize: 30, marginVertical: 20, textAlign: 'center'}}>Desolé !!!</Text>
            <Text style={{color:"black", fontSize: 20, marginVertical: 20, textAlign: 'center'}}>{message_server}</Text>

            <View style={{flexDirection:"row", alignItems:"center", marginVertical: 15}}>
                <Text style={{color: "black", fontSize:10, fontWeight: 'bold'}}>Pour retourner vers la page pricipale</Text>
                    <TouchableOpacity 
                    onPress={()=>{this.props.navigation.navigate("Ligne")}}
                ><Text style={{color: "#16a085" ,fontSize:10, fontWeight: 'bold'}}> Click ici</Text></TouchableOpacity>
            </View>
       </View>
  </View>
    )
  }
}

const MainNavigator = createSwitchNavigator({
  Ligne: {screen: Ligne},
  Destination: {screen: Destination},
  Bus_Exitse: {screen: Bus_Exitse},
  Pas_De_Bus: {screen: Pas_De_Bus}
},
{
  initialRouteName: 'Ligne'
}
);
const App = createAppContainer(MainNavigator);

export default App;

const styles = StyleSheet.create({
    container:{
        flex: 1,
        justifyContent: "center",
        alignItems: "center",
        backgroundColor: "#16a085"
    },
    button:{
        backgroundColor: 'white',
        padding: 8,
        margin: 10,
        justifyContent: 'flex-start',
        alignItems: 'center',
        // borderColor: 'rgba(0, 0, 0, 0.1)',
        // borderRadius:80,
    },
    modalContent:{
         flexDirection: "row",
        backgroundColor: "#16a085",
         padding: 5,
         justifyContent: 'space-around',
         borderRadius: 5,
         borderColor: 'rgba(0, 0, 0, 0.1)',
    }
})