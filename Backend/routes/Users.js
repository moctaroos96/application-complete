const express = require("express");
const users = express.Router();
const cors = require("cors");
const jwt = require("jsonwebtoken");
const bcrypt = require("bcryptjs");

const User = require('../models/User');
users.use(cors());

process.env.SECRET_KEY = 'secret';

users.post('/login', (req,res)=>{
    User.findOne({
       username: req.body.username
    }).then(user=> {
       if(user){
          if(bcrypt.compareSync(req.body.password, user.password)){ 
             const payload = {
                _id: user._id,
                first_name:user.first_name,
                last_name: user.last_name,
                phone_number: user.phone_number,
                username: user.username,
                password: user.password,
                email: user.email
             }       
             let token = jwt.sign(payload, process.env.SECRET_KEY, {
                expiresIn: 1440
             });
                res.json({success: "1", msg: "Identification succcefully....", token: token})            
               //  console.log(token);           
           }else{
             res.json({success: "2", msg: "Incorrect Password"})
            //  console.log("Incorrect Password")
           }  
       }else {
          res.json({success: "3", msg: "user doesnt exist"})    
         //  console.log("user doesnt exist")
       }   
     })
     .catch(err=>{
        console.log(err)
     })
    
    });

    users.post('/register', function (req, res) {  

        const userData = {
           first_name: req.body.first_name,   
           last_name: req.body.last_name,
           phone_number: req.body.phone_number,
           username: req.body.username,   
           password: req.body.password,
           email: req.body.email, 
     
        }
     
        User.findOne({
            username: req.body.username
         }).then((user)=>{
            if(!user){
               bcrypt.hash(req.body.password, 10, function (err,   hash) {
                  userData.password = hash
                  User.create(userData)
                     .then(user=>{
                        console.log(user)
                        res.json({success: "1", msg: 'Registred!'})
                        console.log('registered!')
                     })
                     .catch(err=>{
                        console.log(err)
                        res.json({success: "3", msg: err })
                     })
                  })
               }else{
                  res.json({success: "2", msg: 'Utilisateur existe deja !!'})
                  console.log('User already exists')
              }
          })
          .catch(err=>{
              res.send('error: '+err) 
          })
      })
module.exports = users