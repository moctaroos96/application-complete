const mongoose = require("mongoose")


const UserSchema = mongoose.Schema({
    tripid: {
        type: String
    },
    location:{
        type: Array
    },
    ligne:{
        type: String
    },
    destination :{
        type: String
    },
    date: {
        type: Date
    },
    status: {
        type: String
    }
});

module.exports = Location = mongoose.model('locations', UserSchema)

