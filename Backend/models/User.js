const mongoose = require("mongoose")


const UserSchema = mongoose.Schema({
    first_name:{
        type: String,
        required: true,
    },
    last_name:{
        type: String,
        required: true,
    },
    phone_number:{
        type: Number,
        required: true,
    },
    username:{
        type: String,
        index: true,
        required: true,
        unique: true
    },
    password:{
        type: String,
        required : true
    },
    email:{
        type: String,
        required: true,
    },
});

module.exports = User = mongoose.model('users', UserSchema)

