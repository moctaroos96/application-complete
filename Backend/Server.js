const app = require("express")();
const cors = require("cors");
const bodyParser = require("body-parser");
const http = require('http').Server(app);
const mongoose = require("mongoose");
const io   = require('socket.io')(http);
let tripid001;
app.use(bodyParser.json());
app.use(cors());
app.use(
    bodyParser.urlencoded({
        extended: false
    })
)

const mongoURI = 'mongodb+srv://admin:admin@cluster0-4ymgm.mongodb.net/data0'

// const mongoURI = 'mongodb://127.0.0.1:27017/data0';


mongoose.connect(mongoURI, {useNewUrlParser: true,useFindAndModify: false,useCreateIndex: true })
    .then(()=>console.log("MongoDB connected"))
    .catch(err => console.log(err))

var Users = require('./routes/Users');
app.use('/users', Users);

var Location = require('./models/Location');

app.post('/status0', (req,res)=>{

    Location.countDocuments().exec()
    .then(
        (doc) => {
    new Location({
        tripid: `V${doc+1}`,ligne: req.body.ligne,destination:req.body.destination, date: req.body.date, status: req.body.status}).save()
        tripid001=`V${doc+1}`
        console.log(tripid001)
        res.json({creation: true, tripid_retour: `V${doc+1}`})
        }).catch(
            (err) => {
                console.log(err)
            }
        )   

})


app.post('/status1', (req,res)=>{
    Location.findOneAndUpdate({
        tripid:req.body.tripid
     }, 
     {
           status:"arret"
    }).exec().then((doc)=>{
        console.log(doc);
        res.json({status:true})
    }).catch(
        (err) => {
            console.log(err)
        }
    )   
})

app.post('/demande', (req, res)=>{
    Location.find({ 
        ligne: req.body.ligne,
        destination: req.body.destination,
        status: "en cours"
    }).then(async bus=>{
      if(bus.length>0){
            res.json({success: true, location: bus[0].location[bus[0].location.length-1], tripid: bus[0].tripid});
        }else{
           res.json({success: false, msg : 'Aucun bus correspond a votre demande!!!'});
      }
    }).catch(err=>{
        console.log(err)
     })
})

io.on('connection', (socket)=>{
    console.log('a user connected');
    socket.on(`tripid001`, function(data){
        console.log('data',data);
        tripid002= data.tripid
        io.emit(`tripid002`,data)
         Location.findOneAndUpdate({
            tripid:data.tripid
         }, 
         {$push : {
                location:{
                        latitude: data.data1[0].coords.latitude,
                        longitude: data.data1[0].coords.longitude,
                        heading: data.data1[0].coords.heading,
                        speed: data.data1[0].coords.speed
                }
            }
        }).exec().then((doc) => {
            console.log(doc);
        }).catch((err) => console.log(err))

    })

});


const PORT = process.en || 4000;  
http.listen(PORT, ()=>{
    console.log(`Server is running on port ${PORT}`);
});


