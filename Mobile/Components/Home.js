import React, { Component } from 'react';
import {StyleSheet, Text,TouchableOpacity,View,} from 'react-native';

export default class Home extends Component {
  _login = () => {
    this.props.navigation.navigate("Login")
  }

  _register = () => {
    this.props.navigation.navigate("Register")
  }


  render() {
    return (
      <View style={{flex:1, backgroundColor: "#2C3A47"}}>
        <View style={{justifyContent:"center", alignItems: "center", marginTop: 150}}>
          <Text style={{ fontSize: 40, color: "#ffffff", fontStyle: "italic"}}>WELCOME</Text>
        </View>
        <View style={styles.container}>
          <TouchableOpacity onPress={this._register}>
            <View style={styles.button1}>
              <Text style={styles.buttonText}> (+) CREATE ACCOUNT </Text>
            </View>
          </TouchableOpacity>
          <TouchableOpacity onPress={this._login}>
            <View style={styles.button2}>
              <Text style={styles.buttonText}> (+) LOGIN </Text>
            </View>
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: 60,
    alignItems: "center",
    justifyContent: "flex-end"
  },
  button1: {
    marginBottom: 50,
    width: 365,
    alignItems: 'center',
    backgroundColor: "#25CCF7"
  },
  button2: {
    marginBottom: 50,
    width: 365,
    alignItems: 'center',
    backgroundColor: "#55E6C1"
  },
  buttonText: {
    padding: 30,
    color: 'white',
    fontSize: 20
  }
});




