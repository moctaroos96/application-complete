import Home from './Components/Home';
import Login from './Components/Login';
import Welcome from './Components/Welcome';
import Register from './Components/Register';
import {createSwitchNavigator, createAppContainer} from 'react-navigation';

const MainNavigator = createSwitchNavigator({

  Home: {screen: Home},
  Login: {screen: Login},
  Register: {screen: Register},
  Welcome: {screen: Welcome}
},
{
  initialRouteName: 'Home'
}


);

const App = createAppContainer(MainNavigator);

export default App;


