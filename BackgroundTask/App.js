import React from 'react';
import { Text, TouchableOpacity,View,YellowBox } from 'react-native';
import * as TaskManager from 'expo-task-manager';
import * as Location from 'expo-location';
import * as Permissions from 'expo-permissions';
import Modal from 'react-native-modal';
import SocketIOClient from 'socket.io-client';
import axios from 'axios';

// const socket = SocketIOClient.connect('http://192.168.0.110:8000');

const socket = SocketIOClient.connect('http://bus.boki.tech');

const LOCATION_TASK_NAME = 'background-location-task';

YellowBox.ignoreWarnings([
  'Unrecognized WebSocket connection option(s) `agent`, `perMessageDeflate`, `pfx`, `key`, `passphrase`, `cert`, `ca`, `ciphers`, `rejectUnauthorized`. Did you mean to put these under `headers`?'
]);

let tripid001;

export default class Component extends React.Component {
  constructor(props){
    super(props);
 
    this.state={
      hasLocationPermissions: false,
      visibleModal: null
    }
  }

  async componentDidMount(){
    const { status } = await Permissions.askAsync(Permissions.LOCATION);
    this.setState({ hasLocationPermissions: status === 'granted' });
    
    socket.on('connect', ()=>console.log('Connection'));
  
  }


  onPress = async () => {
    axios.post('http://bus.boki.tech/status0', {
      status: 'en cours',
      date: new Date()
    })
    .then(function (response) {
     if(response.data.creation){
      tripid001=response.data.tripid_retour
      Location.startLocationUpdatesAsync(LOCATION_TASK_NAME, {
        accuracy: Location.Accuracy.Balanced,
        timeInterval: 1000,
      });
     }
    })
    .catch(function (error) {
      console.log(error);
    });

    this.setState({visibleModal: 1})
  };

  onPress1 = async () => {
    axios.post('http://bus.boki.tech/status1', {
      tripid: tripid001
    })
    .then(function (response) {
     if(response.data.status){
      Location.stopLocationUpdatesAsync(LOCATION_TASK_NAME);
     }
    })
    .catch(function (error) {
      console.log(error);
    });

    this.setState({visibleModal: null});
  };

  render() {
    const { hasLocationPermissions } = this.state;
    if (hasLocationPermissions === null) {
      return <Text>Requesting for location permission</Text>;
    }
    if (hasLocationPermissions === false) {
      return <Text>No access to location</Text>;
    }
    return (
     <View style={{flex:1, justifyContent: 'center', alignItems: 'center',backgroundColor: "#222f3e"}}>
        <TouchableOpacity onPress={this.onPress}>
          <View style={{
            backgroundColor: 'white',
            padding: 12,
            margin: 16,
            justifyContent: 'center',
            alignItems: 'center',
            borderRadius: 4,
            borderColor: 'rgba(0, 0, 0, 0.1)',
          }}>
               <Text>Demmarer Un Voyage</Text>
          </View>
        </TouchableOpacity>
        <Modal isVisible={this.state.visibleModal === 1}>
            <View style={{
              backgroundColor: 'white',
              padding: 22,
              justifyContent: 'center',
              alignItems: 'center',
              borderRadius: 4,
              borderColor: 'rgba(0, 0, 0, 0.1)'
            }}>
                  <Text style={{fontSize: 30, fontStyle: "italic"}}>Starting..........!</Text>
                  <TouchableOpacity onPress={this.onPress1}>
                        <View style=
                            {{
                              backgroundColor: 'white',
                              padding: 12,
                              margin: 16,
                              justifyContent: 'center',
                              alignItems: 'center',
                              borderRadius: 4,
                              borderColor: 'rgba(0, 0, 0, 0.1)'
                            }}
                          >
                            <Text>STOP</Text>
                        </View>
                  </TouchableOpacity>
              </View>
           </Modal> 
     </View>
    );
  }
}

TaskManager.defineTask(LOCATION_TASK_NAME, ({ data, error }) => {
  if (error) {
    // Error occurred - check `error.message` for more details.
    return;
  }
  if (data) {
    const { locations } = data;
  
    // do something with the locations captured in the background
    console.log("Locations",locations)
    socket.emit('message',{data1: locations, tripid: tripid001});
  }
});